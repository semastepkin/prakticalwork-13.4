﻿#include <iostream>

const int N = 20;

void printNumbers(int N, bool even) 
{
    std::cout << "Odd numbers from 0 to " << N << ":\n";
    for (int i = even ? 0 : 1; i <= N; i += 2)
    {
        std::cout << i << " ";
    }
    std::cout << std::endl;
}
int main() {
    std::cout << "Even numbers from 0 to " << N << ":\n";
    for (int i = 0; i <= N; i += 2)
    {
        std::cout << i << " ";
    }
    std::cout << std::endl;

    printNumbers(N, false);

    return 0;
}
